class IncomesController < ApplicationController

  helper_method :income

  def index
    @incomes = current_user.incomes.where(:created_at =>  Time.current.all_month).includes(:income_type).page(params[:page]).per(10).reverse_order
  end
  
  def new
    @income = current_user.incomes.new
  end

  def create
    @income = current_user.incomes.build(resource_params)
    if @income.save
      redirect_to new_income_path, notice: "You added #{income.amount} to your Income this month."
    else
      render :new
    end
  end

  def destroy
    income.destroy
    redirect_to incomes_path, notice: "Income #{income.amount} from #{income.income_type.name} is deleted." +
    " Press #{undo_link} to add it back"
  end

  def income
    @income ||= current_user.incomes.find(params[:id])
  end

  private
    def resource_params
      params.require(:income).permit(:income_type_id, :amount, :note)
    end

    def undo_link
      view_context.link_to("undo", revert_version_path(income.versions.scoped.last), :method => :post)
    end
end

class WelcomeController < ApplicationController
  def index
    @last_income = current_user.incomes.last
    @last_expense = current_user.expenses.last
  end
end

class ExpensesController < ApplicationController
  helper_method :expense

  def index
    @expenses = current_user.expenses.where(:created_at =>  Time.current.all_month).includes(:expense_type).page(params[:page]).per(10).reverse_order
  end
  
  def new
    @expense = current_user.expenses.new
  end

  def create
    @expense = current_user.expenses.build(resource_params)
    if @expense.save
      redirect_to new_expense_path, notice: "You added #{expense.amount} to your expense this month."
    else
      render :new
    end
  end

  def destroy
    expense.destroy
    redirect_to expenses_path, notice: "expense #{expense.amount} from #{expense.expense_type.name} is deleted." +
    " Press #{undo_link} to add it back"
  end

  def expense
    @expense ||= current_user.expenses.find(params[:id])
  end

  private
    def resource_params
      params.require(:expense).permit(:expense_type_id, :amount, :note)
    end

    def undo_link
      view_context.link_to("undo", revert_version_path(expense.versions.scoped.last), :method => :post)
    end
end

class ExpenseTypesController < ApplicationController
  helper_method :expense_type
  

  def index
    @expense_types = current_user.expense_types.page(params[:page]).per(15).includes(:parent)
  end
  
  def new
    @expense_type = current_user.expense_types.new
  end

  def create
    @expense_type = current_user.expense_types.build(resource_params)
    if expense_type.save
      redirect_to expense_types_path, notice: "expense Type #{expense_type.name} was saved!"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if expense_type.update(resource_params)
      redirect_to expense_types_path, notice: "Expense Type #{expense_type.name} was saved!"
    else
      render :edit
    end
  end

  def destroy    
      expense_type.destroy
      redirect_to expense_types_path , :notice => "Expense Type #{expense_type.name} deleted!"
  end

  def expense_type
    @expense_type ||= current_user.expense_types.find(params[:id])
  end

  private
    def resource_params
      params.require(:expense_type).permit(:name, :parent_id)
    end
end

class IncomeTypesController < ApplicationController

  helper_method :income_type
  

  def index
    @income_types = current_user.income_types.page(params[:page]).per(15).includes(:parent)
  end
  
  def new
    @income_type = current_user.income_types.new
  end

  def create
    @income_type = current_user.income_types.build(resource_params)
    if income_type.save
      redirect_to income_types_path, notice: "Income Type #{income_type.name} was saved!"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if income_type.update(resource_params)
      redirect_to income_types_path, notice: "Income Type #{income_type.name} was saved!"
    else
      render :edit
    end
  end

  def destroy    
      income_type.destroy
      redirect_to income_types_path , :notice => "Income Type #{income_type.name} deleted!"
  end

  def income_type
    @income_type ||= current_user.income_types.find(params[:id])
  end

  private
    def resource_params
      params.require(:income_type).permit(:name, :parent_id)
    end
end

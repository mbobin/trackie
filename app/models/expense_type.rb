class ExpenseType < ActiveRecord::Base
  validates :name, presence:  true, length: { maximum: 50 }
  validates_uniqueness_of :name, scope: :user_id
  
  belongs_to :parent, class_name: "ExpenseType"
  belongs_to :user
  has_many :children, class_name: "ExpenseType", foreign_key: "parent_id"
  has_many :expenses

  def children_with_parent
    [self].concat self.children
  end
end

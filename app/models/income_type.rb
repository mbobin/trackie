class IncomeType < ActiveRecord::Base
  validates :name, presence:  true, length: { maximum: 50 }
  validates_uniqueness_of :name, scope: :user_id
  
  belongs_to :parent, class_name: "IncomeType"
  belongs_to :user
  has_many :children, class_name: "IncomeType", foreign_key: "parent_id"
  has_many :incomes

  def children_with_parent
    [self].concat self.children
  end

end
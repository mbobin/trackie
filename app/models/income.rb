class Income < ActiveRecord::Base
  validates :income_type, :income_type_id, :amount, presence:  true
  validates_numericality_of :amount, :greater_than_or_equal_to => 0
  belongs_to :income_type
  belongs_to :user
  has_paper_trail
end

class Expense < ActiveRecord::Base
  validates :expense_type, :expense_type_id, :amount, presence:  true
  validates_numericality_of :amount, :greater_than_or_equal_to => 0
  belongs_to :expense_type
  belongs_to :user
  has_paper_trail
end

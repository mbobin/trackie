class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable, :recoverable,
    :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]
    
  has_many :income_types, dependent: :destroy
  has_many :incomes, dependent: :destroy
  has_many :expense_types, dependent: :destroy
  has_many :expenses, dependent: :destroy
  has_many :invitations, :class_name => self.to_s, :as => :invited_by, dependent: :destroy

  validates :name, :email, presence: true
  validates :username, uniqueness: true
  validates :email, uniqueness: true
  
  validates :username, length: {minimum: 4,
    :message => "The username should have more then 4 characters."}
  
  validates :username, length: {maximum: 50,
    :message => "The username you are trying to use has more then 50 characters.
    Probably you will have a hard time to remember it so please choose one with fewer characters."}
  
   validate :password_complexity

  def password_complexity
    if password.present? and not 
      password.match(/(?:(?=.*[a-z])((?=.*[A-Z])|(?=.*[\W]))).{8,}/)
      errors.add :password, "The password you wrote is unsecured. It should have at least one uppercase 
        characters or one non-alphanumeric character."
    end
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

   def self.find_first_by_auth_conditions(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      else
        where(conditions).first
      end
    end

  def self.find_for_facebook_oauth(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name
      user.username = auth.info.nickname || auth.uid
      user.skip_confirmation!
      user.save!
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.name = data["name"] if user.name.blank?
        user.email = data["email"] if user.email.blank?
      end
    end
  end

end

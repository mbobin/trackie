module ExpensesHelper
  def expense_type_collection
    current_user.expense_types.where('parent_id is ?', nil).includes(:children)
  end
end

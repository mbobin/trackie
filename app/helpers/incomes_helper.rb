module IncomesHelper
  def income_type_collection
    current_user.income_types.where('parent_id is ?', nil).includes(:children)
  end
end

module IncomeTypeHelper
  def delete_button income_type
    if income_type.children.any? or income_type.incomes.any?
      link_to 'Delete', '#',
      :disabled => true,
      :class => 'btn btn-mini btn-danger',
      title: 'You must delete all children and all incomes of this type.', rel: 'tooltip'
    else
      link_to 'Delete',
      income_type_path(income_type),
      :method => :delete,
      :data => { :confirm => "Press Ok to Delete the Income Type #{income_type.name} ?"},
      :class => 'btn btn-mini btn-danger'
    end
  end

  def select_income_types
    if income_type.children.any?
      return current_user.income_types.none
    end
    current_user.income_types.where('parent_id is ? AND id is not ? ', nil, income_type.id)
  end
  
end

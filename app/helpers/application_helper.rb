module ApplicationHelper
  def monthly_expense
    current_user.expenses.where(:created_at =>  Time.current.all_month).sum(:amount)
  end

  def monthly_income
    current_user.incomes.where(:created_at =>  Time.current.all_month).sum(:amount)
  end

  def balance
    @balance = monthly_income - monthly_expense
  end

end

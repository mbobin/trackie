module ExpenseTypesHelper
    def delete_button_for expense_type
    if expense_type.children.any? or expense_type.expenses.any?
      link_to 'Delete', '#',
      :disabled => true,
      :class => 'btn btn-mini btn-danger',
      title: 'You must delete all children and all expenses of this type.', rel: 'tooltip'
    else
      link_to 'Delete',
      expense_type_path(expense_type),
      :method => :delete,
      :data => { :confirm => "Press Ok to Delete the Expense Type #{expense_type.name} ?"},
      :class => 'btn btn-mini btn-danger'
    end
  end

  def select_expense_types
    if expense_type.children.any?
      return current_user.expense_types.none
    end
    current_user.expense_types.where('parent_id is ? AND id is not ? ', nil, expense_type.id)
  end

end

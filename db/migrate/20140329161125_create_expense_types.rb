class CreateExpenseTypes < ActiveRecord::Migration
  def change
    create_table :expense_types do |t|
      t.string :name

      t.timestamps
    end
    add_column :expense_types, :parent_id, :integer
  end
end

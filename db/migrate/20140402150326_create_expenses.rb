class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.references :expense_type, index: true
      t.integer :amount
      t.text :note

      t.timestamps
    end
  end
end

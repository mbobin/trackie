class CreateIncomeTypes < ActiveRecord::Migration
  def change
    create_table :income_types do |t|
      t.string :name
      
      t.timestamps
    end
    add_column :income_types, :parent_id, :integer
  end
end

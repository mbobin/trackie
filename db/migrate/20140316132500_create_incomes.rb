class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.text :note
      t.integer :amount
      t.references :income_type

      t.timestamps
    end
      add_index :incomes, :income_type_id
  end
end
